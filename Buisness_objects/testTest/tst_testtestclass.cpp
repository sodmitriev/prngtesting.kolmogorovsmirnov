#include <QtTest>
#include <iostream>
#include "../test.h"

class TestTestclass : public QObject
{
    Q_OBJECT

    bool seqequal(vector<Sequence> seq1, vector<Sequence> seq2);
    bool seqequal(Sequence seq1, Sequence seq2);
public:
    TestTestclass();

private Q_SLOTS:
    void testConstructor();
    void testInitConstructor();
    void testgetSignLevel();
    void testgetSignValue();
    void testgetStats();
    void testgetSequences();
    void testgetSequence();
    void testgetSequence_failed();
    void testaddSequence();
    void testdeleteSequence();
    void testdeleteSequence_failed();
    void testrecalcStats();
};

TestTestclass::TestTestclass() {}

void TestTestclass::testConstructor(){
    Test test1(0.005), test2(0.025), test3(0.10);

    QVERIFY((test1.signlevel - 0.005) < 0.001);
    QVERIFY((test1.signvalue - 1.73) < 0.01);
    QVERIFY((test2.signlevel - 0.025) < 0.001);
    QVERIFY((test2.signvalue - 1.48) < 0.01);
    QVERIFY((test3.signlevel - 0.10) < 0.001);
    QVERIFY((test3.signvalue - 1.22) < 0.01);
}

void TestTestclass::testInitConstructor(){
    vector<int> data0({0, 0, 0, 0}), data({10, 3, 25, 22, 8, 6, 14, 12});
    Sequence seq(0), seq1(data0, vector<double>()), seq2(data, vector<double>());
    seq1.buildGraph();
    seq2.buildGraph();
    vector<Sequence> vs, vs1({seq, seq1, seq2});
    vector<double> st, st1({0.2, 0.1, 0.003, 0.076});
    Test tst(0.001, vs, st), tst1(0.05, vs1, st), tst2(0.1, vs1, st1);

    QVERIFY((tst.signlevel - 0.001 < 0.001) && (tst.signvalue - 1.95 < 0.01));
    QVERIFY(seqequal(tst.sequences, vs) && (tst.statresults == st));
    QVERIFY((tst1.signlevel - 0.05 < 0.001) && (tst1.signvalue - 1.36 < 0.01));
    QVERIFY(seqequal(tst1.sequences, vs1) && (tst1.statresults == st));
    QVERIFY((tst2.signlevel - 0.1 < 0.001) && (tst2.signvalue - 1.22 < 0.01));
    QVERIFY(seqequal(tst2.sequences, vs1) && (tst2.statresults == st1));
}

void TestTestclass::testgetSignLevel() {
    Test test1(0.005), test2(0.05), test3(0.10);

    QVERIFY((test1.getSignLevel() - 0.005) < 0.001);
    QVERIFY((test2.getSignLevel() - 0.05) < 0.001);
    QVERIFY((test3.getSignLevel() - 0.10) < 0.001);
}

void TestTestclass::testgetSignValue() {
    Test test1(0.005), test2(0.05), test3(0.10);

    QVERIFY((test1.getSignValue() - 1.73) < 0.01);
    QVERIFY((test2.getSignValue() - 1.36) < 0.01);
    QVERIFY((test3.getSignValue() - 1.22) < 0.01);
}

void TestTestclass::testgetStats() {
    vector<double> st, st1({0.2, 0.1, 0.003, 0.076});
    Test tst(0.04), tst1(0.2, vector<Sequence>(), st), tst2(0.007, vector<Sequence>(), st1);

    QVERIFY(tst.getStats() == st);
    QVERIFY(tst1.getStats() == st);
    QVERIFY(tst2.getStats() == st1);
}

void TestTestclass::testgetSequences() {
    vector<int> data0({0, 0, 0, 0}), data({10, 3, 25, 22, 8, 6, 14, 12});
    Sequence seq(0), seq1(data0, vector<double>()), seq2(data, vector<double>());
    seq1.buildGraph();
    seq2.buildGraph();
    vector<Sequence> vs, vs1({seq, seq1, seq2});
    vector<double> st;
    Test tst(0.1), tst1(0.075, vs, st), tst2(0.64, vs1, st);

    QVERIFY(seqequal(tst.getSequences(), vs));
    QVERIFY(seqequal(tst1.getSequences(), vs));
    QVERIFY(seqequal(tst2.getSequences(), vs1));
}

void TestTestclass::testgetSequence() {
    vector<int> data0({0, 0, 0, 0}), data({10, 3, 25, 22, 8, 6, 14, 12});
    Sequence seq(0), seq1(data0, vector<double>()), seq2(data, vector<double>());
    seq1.buildGraph();
    seq2.buildGraph();
    vector<Sequence> vs({seq, seq1, seq2});
    Test tst(0.73, vs, vector<double>());

    QVERIFY(seqequal(tst.getSequence(0), seq));
    QVERIFY(seqequal(tst.getSequence(1), seq1));
    QVERIFY(seqequal(tst.getSequence(2), seq2));
}

void TestTestclass::testgetSequence_failed() {
    vector<int> data0({0, 0, 0, 0}), data({10, 3, 25, 22, 8, 6, 14, 12});
    Sequence seq(0), seq1(data0, vector<double>()), seq2(data, vector<double>());
    seq1.buildGraph();
    seq2.buildGraph();
    vector<Sequence> vs({seq, seq1, seq2});
    Test tst(0.54), tst1(0.73, vs, vector<double>());

    QVERIFY_EXCEPTION_THROWN(tst.getSequence(0), test_sequence_index_error);
    QVERIFY_EXCEPTION_THROWN(tst1.getSequence(3), test_sequence_index_error);
    QVERIFY_EXCEPTION_THROWN(tst1.getSequence(27), test_sequence_index_error);
}

void TestTestclass::testaddSequence() {
    vector<int> data0({0, 0, 0, 0}), data({10, 3, 25, 22, 8, 6, 14, 12});
    Sequence seq(0), seq1(data0, vector<double>()), seq2(data, vector<double>());
    seq1.buildGraph();
    seq2.buildGraph();
    vector<Sequence> vs({seq1});
    Test tst(0.54), tst1(0.73, vs, vector<double>());

    tst.addSequence(seq1);
    QVERIFY((tst.sequences.size() == 1) && seqequal(tst.getSequence(0), seq1));
    tst.addSequence(seq2);
    QVERIFY((tst.sequences.size() == 2) && seqequal(tst.getSequence(1), seq2));
    tst1.addSequence(seq);
    QVERIFY((tst1.sequences.size() == 2) && seqequal(tst1.getSequence(1), seq));
    tst1.addSequence(seq2);
    QVERIFY((tst1.sequences.size() == 3) && seqequal(tst1.getSequence(2), seq2));
}

void TestTestclass::testdeleteSequence() {
    vector<int> data0({0, 0, 0, 0}), data({10, 3, 25, 22, 8, 6, 14, 12});
    Sequence seq(0), seq1(data0, vector<double>()), seq2(data, vector<double>());
    seq1.buildGraph();
    seq2.buildGraph();
    vector<Sequence> vs({seq, seq1, seq2}), vs1({seq, seq2}), vs2({seq}), vs0;
    Test tst(0.73, vs, vector<double>());

    tst.deleteSequence(1);
    QVERIFY(tst.sequences.size() == 2 && seqequal(tst.getSequences(), vs1));
    tst.deleteSequence(1);
    QVERIFY(tst.sequences.size() == 1 && seqequal(tst.getSequences(), vs2));
    tst.deleteSequence(0);
    QVERIFY(tst.sequences.size() == 0 && seqequal(tst.getSequences(), vs0));
}

void TestTestclass::testdeleteSequence_failed() {
    vector<int> data({10, 3, 25, 22, 8, 6, 14, 12});
    Sequence seq(0), seq1(data, vector<double>());
    seq1.buildGraph();
    vector<Sequence> vs({seq, seq1});
    Test tst(0.3), tst1(0.73, vs, vector<double>());

    QVERIFY_EXCEPTION_THROWN(tst.deleteSequence(0), test_sequence_index_error);
    QVERIFY_EXCEPTION_THROWN(tst1.deleteSequence(2), test_sequence_index_error);
    QVERIFY_EXCEPTION_THROWN(tst1.deleteSequence(45), test_sequence_index_error);
}

void TestTestclass::testrecalcStats() {
    vector<int> data({9, 55, 30, 18}), data1({10, 3, 25, 22}), data2({16, 28, 9, 52});
    Sequence seq(data, vector<double>()), seq1(data1, vector<double>()), seq2(data2, vector<double>());
    seq.buildGraph();
    seq1.buildGraph();
    seq2.buildGraph();
    vector<Sequence> vs({seq, seq1, seq2});
    Test tst(0.73, vs, vector<double>());

    tst.recalcStats();
    QVERIFY(tst.statresults[0] - 0.35 < 0.01);
    QVERIFY(tst.statresults[1] - 0.33 < 0.01);
    QVERIFY(tst.statresults[2] - 0.20 < 0.01);
}


bool TestTestclass::seqequal(vector<Sequence> seq1, vector<Sequence> seq2) {
    bool flag = (seq1.size() == seq2.size());
    if (flag) {
        for (unsigned int i = 0; i < seq1.size(); ++i)
            if (seq1[i].data != seq2[i].data || seq1[i].graph != seq2[i].graph)
                flag = 0;
    }
    return flag;
}



bool TestTestclass::seqequal(Sequence seq1, Sequence seq2) {
    bool flag = (seq1.getSize() == seq2.getSize());
    if (flag) {
        flag = (seq1.data == seq2.data || seq1.graph == seq2.graph);
    }
    return flag;
}

QTEST_APPLESS_MAIN(TestTestclass)

#include "tst_testtestclass.moc"
