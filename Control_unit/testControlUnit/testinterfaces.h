#ifndef TESTINTERFACES_H
#define TESTINTERFACES_H

#include "../controlinterface.h"
#include "../../Buisness_objects/sequence.h"
#include "../../Buisness_objects/test.h"
#include "../fileinterface.h"
#include "../databaseinterface.h"
#include <vector>
#include <string>
#include <utility>

using std::vector;
using std::string;

/*class testControlInterface: public ControlInterface {
public:
    testControlInterface() {}
    //0 - good, 1 - bad parameters, 2 - bad sequence file, 3 - bad signlevel, 4 - bad output file
    virtual int Create_test(int size, int separator, double signlevel, string outfile, vector<string> infiles) override;
    //5 - no test, 6 - bad output file
    virtual int Show_test_info(unsigned int index, string outfile) override;
};*/

class testDatabaseInterface: public DatabaseInterface {
public:
    unsigned int index;
    Test test;

    testDatabaseInterface(): test() {}
    virtual unsigned int Get_last_index() override;
    virtual bool Search_test(unsigned int index, Test &test) override;
    virtual void Save_test(unsigned int index, const Test &test) override;
};

class testFileInterface: public FileInterface {
    string filename;
    int size;
    int separator;

public:
    vector<int> data;
    int i;

    testFileInterface(): i(0), data() {}
    virtual bool Open_file(string filename, int size, int separator) override;
    virtual bool Get_number(int &number) override;
    virtual void Close_file() override;

    friend class ControlUnitTest;
};

#endif // TESTINTERFACES_H

