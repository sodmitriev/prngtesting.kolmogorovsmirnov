#-------------------------------------------------
#
# Project created by QtCreator 2018-11-15T14:49:00
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_controlunittest
CONFIG += console
CONFIG -= app_bundle
CONFIG += c++11
LIBS += -lgcov
QMAKE_CXXFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
QMAKE_LDFLAGS += -g -Wall -fprofile-arcs -ftest-coverage  -O0

TEMPLATE = app

HEADERS += \
    "../*.h" \
    "../../Buisness_objects/*.h" \
    testinterfaces.h

SOURCES += tst_controlunittest.cpp\
    "../*.cpp" \
    "../../Buisness_objects/*.cpp" \
    testinterfaces.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"
