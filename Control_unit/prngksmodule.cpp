#include "controlinterface.h"
#include "controlunit.h"
#include "databaseinterface.h"
#include "fileinterface.h"
#include "./FileAccess/fileaccess.h"
#include "./testControlUnit/testinterfaces.h"
#include <string>
#include <fstream>
#include <cstdlib>
#include <iostream>

using std::stoi;

//0 - OK
//1 - bad params
int main(int argc, char* argv[]) {
    FileInterface *fl = new FileAccess;
    DatabaseInterface *db = new testDatabaseInterface;
    ControlInterface *cu = new ControlUnit(0, db, fl);

    if (argc < 6) return 1;
    int size = stoi(argv[1], nullptr, 10), separator = stoi(argv[2], nullptr, 10);
    if (!cu->Check_params(size, separator))
        return 2;

    for (int i = 5; i < argc; ++i)
        if (!cu->Load_sequence(argv[i], size, separator))
            return 3;

    double signlevel = strtod(argv[3], NULL), signvalue = 0;
    vector<double> stats;
    vector<vector<double> > graph;
    if (!cu->Build_test(signlevel, signvalue, stats, graph))
        return 4;

    std::ofstream fout(argv[4]);
    fout << signlevel << ' ' << signvalue << std::endl;
    for (unsigned int i = 0; i < stats.size(); ++i)
        fout << stats[i] << ' ';
    fout << std::endl;
    for (unsigned int i = 0; i < graph.size(); ++i) {
        for (unsigned int j = 0; j < graph[i].size(); ++j)
            fout << graph[i][j] << ' ';
        fout << std::endl;
    }
}
