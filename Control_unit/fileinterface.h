#ifndef FILEINTERFACE_H
#define FILEINTERFACE_H

#include <string>

using std::string;

class FileInterface {
    string filename;
    int size;
    int separator;

public:
    FileInterface() {}
    virtual bool Open_file(string filename, int size, int separator) = 0;
    virtual bool Get_number(int &number) = 0;
    virtual void Close_file() = 0;
};

#endif // FILEINTERFACE_H
