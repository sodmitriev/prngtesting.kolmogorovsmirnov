#ifndef CONTROLINTERFACE_H
#define CONTROLINTERFACE_H

#include <vector>
#include <string>

using std::vector;
using std::string;

class ControlInterface {
public:
    ControlInterface() {}

    //Return false if wrong size or separator
    //size - amount of bits in the sequence numbers, 0 < size <= 16
    //separator - amount of bits in separation symbols, 0 <= separator
    virtual bool Check_params(int size, int separator) = 0;

    //Return false, if wrong file name; load sequence from file
    virtual bool Load_sequence(string file, int size, int separator) = 0;

    //Return false, if wrong signlevel, 0 < signlevel < 1
    //Create and show test with sequences was loading before, after that clear sequences
    virtual bool Build_test(double signlevel, double &signvalue, vector<double> &stats, vector<vector<double> > &graph) = 0;

    //Return false, if no such test
    //Search and show information about test with appropriate index
    virtual bool Show_test(unsigned int index, double &signlevel, double &signvalue, vector<double> &stats, vector<vector<double> > &graph) = 0;
};

#endif // CONTROLINTERFACE_H
